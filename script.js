$(document).ready(() => {
    /* Login animation */
    $('.login-button').on('click', () => {
      $('.login-form').toggle();

      $('.login-form').on('mouseenter', function() {
        $('.login-form').show();
      });

      $('.login-form').on('mouseleave', function() {
        $('.login-form').hide();
      });
    });
    
    /* Menu animation */
    $('.menu-button').on('mouseenter', () => {
      $('.nav-menu').show();
      $('.menu-button').css({
        color: '#C3FF00',
        backgroundColor: '#535353',
      });

      $('.menu-button').animate({
        fontSize: '24px'
      }, 200);
    });
    
    $('.nav-menu').on('mouseleave', () => {
      $('.nav-menu').hide();
      $('.menu-button').css({
        color: '#EFEFEF',
        backgroundColor: '#303030',
        'padding': '.3em 1.25em'
      });
      
      $('.menu-button').animate({
        fontSize: '18px'
      }, 200);
    });
    
    /* Increases image size */
    $('.product-photo').on('mouseenter', event => {
      $(event.currentTarget).addClass('photo-active')
    }).on('mouseleave', event => {
      $(event.currentTarget).removeClass('photo-active')
    });

    /* Reveals more shoe details */
    $('.more-details-button').on('click', event => {
      $(event.currentTarget).closest('.product-details').next().toggle();

      $(event.currentTarget).find('img').toggleClass('rotate');
    });  

    /*Changes color of menu button */
    $('.menu-button').on('mouseenter', () => {
      $('.menu-button').css('color', '#C3FF00');
      $('.nav-menu').show();

      $('.nav-menu ul li').on('mouseenter', () => {
        $('.nav-menu ul li:hover').css({'color': '#C3FF00', 'cursor': 'pointer'});
      });

      $('.nav-menu ul li').on('mouseout', () => {
        $('.nav-menu ul li').css('color', '#EFEFEF')
      });

    $('.nav-menu').on('mouseleave', () => {
      $('.nav-menu').hide();
      $('.menu-button').css('color', '#EFEFEF')
    });

    /* Activated the Add To Cart button */
    $('.shoe-details li').on('click', event => {
      $(event.currentTarget).addClass('active');
      $(event.currentTarget).siblings().removeClass('active');
      $('.shoe-details').children().removeClass('disabled');
    });
  });
});